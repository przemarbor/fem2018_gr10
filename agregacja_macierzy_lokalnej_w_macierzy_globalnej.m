function A =  agregacja_macierzy_lokalnej_w_macierzy_globalnej(M, nr_glob_wezlow_elem, A)
  %funkcja przyjmuje macierz lokalna M, numery wezlow w macierzy [lewa strona, prawa strona], oraz dotychczas wypelniona macierz A
  l=nr_glob_wezlow_elem(1);
  p=nr_glob_wezlow_elem(2);
  

  A(l,l)=M(1,1)+A(l,l);
  A(l,p)=M(1,2)+A(l,p);
  A(p,l)=M(2,1)+A(p,l);
  A(p,p)=M(2,2)+A(p,p);
  
 end