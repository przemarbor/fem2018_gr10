function [A b ] =  allocate(nr_nodes)
    A=zeros(nr_nodes,nr_nodes); 
    b=zeros(nr_nodes);    
end