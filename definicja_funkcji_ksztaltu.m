function [ lokalne_fun_ksztaltu , pochodne_lokalnych_fun_ksztaltu ] = definicja_funkcji_ksztaltu (nr_nodes,p )
    %wezly [numery globalne,i wartosc poczatkowa??]
    lokalne_fun_ksztaltu=cell(nr_nodes,2);
    pochodne_lokalnych_fun_ksztaltu=cell(nr_nodes,2);
    a=zeros(nr_nodes,2);
    b=zeros(nr_nodes,2);
    for i = 1:nr_nodes(end-1)

        
        
        %%%%%%%%%%%%%%%%%%%%%%   funkcja rosnaca   %%%%%%%%%%%%%%%%%
        y1=1;
        y2=0;
        x1=p(i);
        x2=p(i+1);
        a(i,1)=(y1-y2)/(x1-x2);
        b(i,1)=y1-a(i,1)*x1;
        y=@(x)a(i,1)*x+b(i,1);                                
        
        lokalne_fun_ksztaltu{i,1}=y;
        pochodne_lokalnych_fun_ksztaltu{i,1}=a;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %%%%%%%%%%%%%%%%%%%%%%   funkcja malejaca   %%%%%%%%%%%%%%%%
        y1=0;
        y2=1;
        x1=p(i);
        x2=p(i+1);
        a(i,2)=(y1-y2)/(x1-x2);
        b(i,2)=y1-a(i,2)*x1;
        y=@(x)a(i,2)*x+b(i,2);
        
        lokalne_fun_ksztaltu{i,2}=y;
        pochodne_lokalnych_fun_ksztaltu{i,2}=a;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    end

end