function [nodes, elements] = geometria(l,s)
       
       h=s;                      
        p=[0:h:l]';                         
        nr_nodes = length(p); 
  
  for i=1:nr_nodes
      nodes(i,1) = i; %global numbers 
      nodes(i,2) = p(i); %rzeczywista pozycja noda 
  end   
  
  for i = 2:nr_nodes      
      e(i-1,:) = [i-1 i];       
  end
  for i=2:nr_nodes
    elements(i,1) = [i,i];% globaly nr_elemetu
    elements(i,2) = e(i-1,i);

end