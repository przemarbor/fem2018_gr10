%1D problem przewodzenia ciep�a w pr�cie

clear all
clc
% inicjalizacja parametrow sterujacych programem OSOBA 1

            % parametry steruj�ce

% wlasnocci materialu 
P=1;                       % przewodnoc ciapla paterialu
pc=1;                      % poojemnoc cieplna
fi=1;                       % przekroj pr�ta
units=2;                   % dlugosc preta

        %tworze geometrie

%parametry  potrzebne do utwozena gromertii

h=0.1;                      % dystans mi�dzy wezami
p=[0:h:units]';             
x = [0:h:units];            % Przestrzenna dyskretyzacja
numberOfNodes = length(p);     
nr_global  = [1:numberOfNodes];    %pozycja nodow
% tworze macierz elemetow o rownych elemetach  dlugosci = h
  for i = 2:numberOfNodes       
      e(i-1,:) = [i-1 i];
  end
  
  % warunki brzegowe

WB(1,1) =0;         %tu chyba wartosci tych war. brzeg
first=1;
WB(1,2) = p(first); 
WB(2,1) = units;    %tu chyba wartosci tych war. brzeg
last=length(p);
WB(2,2) = p(last); 

nr_elements =length(e);

%koniec (osoba1)


% - w tym miejscu funkcja do wyswietlana grometri (osoba 2)
rysuj_geometrie(p,e,WB);

%- aklokacja pamieci (osoba 3)
[A b]  =allocate(numberOfNodes);

% - procesing 
[ lokalne_fun_ksztaltu , pochodne_lokalnych_fun_ksztaltu ] = definicja_funkcji_ksztaltu (nr_global,p);

for k=1:nr_elements  
    
[nr_glob_elem , nr_glob_wezlow_elem , wspolrzedne_wezlow , jakobian]= zgromadzenie_informacji_dotyczacych_elemetow(k, h);

M = obliczenie_lokalnej_macierzy_opisujacej_element ( k,pochodne_lokalnych_fun_ksztaltu, lokalne_fun_ksztaltu,wspolrzedne_wezlow);    
A = agregacja_macierzy_lokalnej_w_globalnej(M, e, A);

b =  obliczanie_elementow_wektora_prawej_strony (b,pochodne_lokalnych_fun_ksztaltu,warunek_brzegowy,wezly,k);
end 

% uwzgl�dnienie warunk�w brzegowych (osoba 7)
[ A , b] = uwzglednienie_warunkow_brzegowych(WB , A , b) ;

a = A\b;
%  obrobka_wynikow OSOBA 2
rysuj_rozwiazanie(p,a);




