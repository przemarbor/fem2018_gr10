function b =  obliczanie_elementow_wektora_prawej_strony (b,pochodne_lokalnych_fun_ksztaltu,warunek_brzegowy,wezly,k)
    %zmiana dla ulatwienia pracy innych
    rozm_tab=size(wezly);

    %wartosci poczatkowe zmiennych poszukiwanych i potrzebnych do szukania
    max=wezly(1,3);
    min=wezly(1,3);
    wezel_ostatni=1;
    wezel_pierwszy=1;

    % petla do znalezienia wezlow brzegowych: 

    for n=1:rozm_tab(1)
        if max<wezly(n,3)
            max=wezly(n,3);
            wezel_ostatni=n;
        end
        if min>wezly(n,2)
            min=wezly(n,2);
            wezel_pierwszy=n;
        end
    end
    
    %wartosci domyslne dla wezlow sasiadujacych z brzegowymi
    wezel_drugi=0;
    wezel_przedostatni=0;
    
    %znalezienie wartozic wenzlow derugiego i przedostatniego
    for n=1:rozm_tab_wezly(1)                             
        if(tab_wezly(wezel_min,3)==tab_wezly(n,2))     
            wezel_drugi=n;
        end
        if(tab_wezly(wezel_max, 2)==tab_wezly(n,3))
            wezel_przedostatni=n;
        end
    end

    % obliczanie wektora b:

    x=pochodne_lokalnych_fun_ksztaltu(k);
    b(k)=warunek_brzegowy(1)*x(wezel_pierwszy)+warunek_brzegowy(2)*x(wezel_ostatni); 
end
