function M = obliczenie_lokalnej_macierzy_opisujacej_element ( k,pochodne,lok_fun_kszaltu,wspolrzedne)

M(1,1)= quad(pochodne(k,1)*pochodne(k,1)+lok_fun_kszaltu(k,1)*lok_fun_kszaltu(k,1),wspolrzedne(1),wspolrzedne(2));
M(1,2)= quad(pochodne(k,2)*pochodne(k,1)+lok_fun_kszaltu(k,2)*lok_fun_kszaltu(k,1),wspolrzedne(1),wspolrzedne(2));
M(2,1)= quad(pochodne(k,1)*pochodne(k,2)+lok_fun_kszaltu(k,1)*lok_fun_kszaltu(k,2),wspolrzedne(1),wspolrzedne(2));
M(2,2)= quad(pochodne(k,2)*pochodne(k,2)+lok_fun_kszaltu(k,2)*lok_fun_kszaltu(k,2),wspolrzedne(1),wspolrzedne(2));

end