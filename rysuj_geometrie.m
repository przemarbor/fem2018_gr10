function rysuj_geometrie(p,elementy,WB)

	figure('Name','Wezly','NumberTitle','off')
	plot([0 WB(2,2)],[0 0],p,0,'o');grid;

	figure('Name','Elementy','NumberTitle','off')
	hold on
	for k = 1:length(elementy)
            x1_el = (elementy(k,1)-1)/10;
    		x2_el = (elementy(k,2)-1)/10;
    		plot([x1_el x2_el],[0 0],'-*');
	end
	grid;
	
	figure('Name','War_Graniczne','NumberTitle','off')
	plot(WB(1,2),WB(1,1),'o',WB(2,2),WB(2,1),'o',[WB(1,2)+0.01 WB(2,1)-0.01],[0 0]);grid;
	
end