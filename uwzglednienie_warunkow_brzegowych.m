function [ A , b] = uwzglednienie_warunkow_brzegowych(WARUNKI_BRZEGOWE , A , b) 
%wypelnienie pierwszego wiersza mecierzy A jedynk� na miesjcu (1,1) i 
%zerami w pozosta�ych kolumnach
x = size(A);
x = x(1, 1);
A(1, 1) = 1;
while(x > 1)
    A(1, x) = 0;
    x = x - 1;
end
%wypelnienie ostatniego wiersza mecierzy A jedynk� na miesjcu (y,x) i
%zerami w pozosta�ych kolumnach
x = size(A);
y = x(1, 2);
x = x(1, 1);

A(y, x) = 1;
x = x - 1;
while(x > 0)
    A(y, x) = 0;
    x = x - 1;
end
%uwzglednienie warunkow brzegowych w wektorze b
x = size(b);
x = x(1, 1);
y = size(WARUNKI_BRZEGOWE);
y = y(1, 2);
b(1, 1) = WARUNKI_BRZEGOWE(1, 1);
b(x, 1)= WARUNKI_BRZEGOWE(1, y);

end

