 function [nr_glob_elem , nr_glob_wezlow_elem , wspolrzedne_wezlow , jakobian]= zgromadzenie_informacji_dotyczacych_elemetow(k, h)
    nr_glob_elem=k;
    nr_glob_wezl_elem=[k-1 k];
    wspolrzedne_wezlow=[(k-1)*h k*h];
    jakobian=(wspolrzedne_wezlow(2)-wspolrzedne_wezlow(1))/2;
  
  end